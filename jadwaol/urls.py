from django.contrib import admin 
from django.urls import path, include
from .views import index, tampil, delete, indexdelete, lihatjadwal, schedule_delete
    
urlpatterns = [
    path('',index, name='tambah'),
    path('tampil/',tampil, ),
    path('delete/', indexdelete, name='delete'),
    path('deletejadwal/', delete),
    path('lihatjadwal/', lihatjadwal, name='tampil' ),
    path('deletejadwal/<int:id>/', schedule_delete, name='schedule_delete'),
]
