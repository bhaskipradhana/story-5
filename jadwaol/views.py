from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import inputjadwal
from .models import simpanjadwalmodels
# Create your views here.

def lihatjadwal(request):
    all = simpanjadwalmodels.objects.all()
    argument = {
        'sebelum' : all
    }
    return render(request, 'tampil.html', argument)

def index(request):
    formulir = inputjadwal()
    arg = {
        'formulir': formulir,
    }
    return render(request, 'index.html', arg)

def indexdelete(request):
    formulir = deleteJadwal()
    arg = {
        'formulir' : formulir
    }
    return render(request, 'delete.html', arg)

def tampil(request):
    getdate = request.POST['date_kegiatan_day']
    getmonth = request.POST['date_kegiatan_month']
    getyear = request.POST['date_kegiatan_year']
    gettime = request.POST['time_kegiatan']
    getnama = request.POST['nama_kegiatan']
    gettempat = request.POST['tempat_kegiatan']
    getkegiatan = request.POST['kategori_kegiatan']

    monthdict={
        '1' : 'January',
        '2' : 'February',
        '3' : 'March',
        '4' : 'April',
        '5' : 'May',
        '6' : 'June',
        '7' : 'July',
        '8' : 'August',
        '9' : 'September',
        '10': 'November',
        '11': 'November',
        '12' : 'December',
    }

    jadwalbaru = simpanjadwalmodels(
        simpannama = getnama,
        simpandate = getdate,
        simpanmonth = monthdict[getmonth],
        simpanyear = getyear,
        simpantime = gettime,
        simpantempat = gettempat,
        simpankategori = getkegiatan,
    )
    jadwalbaru.save()
    all = simpanjadwalmodels.objects.all()
    arg = {
        'sebelum' : all
    }
    return render(request,'tampil.html', arg)

# def delete(request):
#     hasildelete = request.POST['namadelete']
#     all = simpanjadwalmodels.objects.all()

#     for i in all:
#         if i.simpanhari == hasildelete:
#             i.delete()

#     all = simpanjadwalmodels.objects.all()

#     arg = {
#         'sebelum' : all
#     }
#     return render(request, 'tampil.html', arg)
def delete(request, id):
    all = simpanjadwalmodels.objects.get(id=id)
    simpanjadwalmodels.delete()
    return redirect('simpanjadwalmodels')

def schedule_delete(request, id):
    simpanjadwalmodels.objects.filter(id=id).delete()
    all = simpanjadwalmodels.objects.all()
    arg = {
        'sebelum' : all
    }
    return render(request,'tampil.html', arg)


