from django.db import models
from datetime import datetime

class simpanjadwalmodels(models.Model):
    simpannama = models.CharField(default="", max_length=20)
    simpandate = models.IntegerField(default = 0)
    simpanmonth = models.CharField(max_length = 20, default = " ")
    simpanyear = models.IntegerField(default = 0)
    simpantime = models.CharField(default="", max_length=5)
    simpantempat = models.CharField(default="", max_length=20)
    simpankategori = models.CharField(default="", max_length=20)
    
# Create your models here.
