# from django import forms
# class inputjadwal(forms.Form):
#     hari = forms.CharField(label="Hari:", max_length=20)
#     tanggal = forms.DateTimeField(label="Tanggal:",)

# class deleteJadwal(forms.Form):
#     namadelete = forms.CharField(label='Nama yang dihapus:')

from django import forms
from .models import simpanjadwalmodels


# class ScheduleForm(forms.ModelForm):
#     class Meta:
#         model = simpanjadwalmodels
#         fields = ['activity', 'date', 'time', 'place', 'category']
#         widgets = {
#             'date': forms.DateInput(attrs={'type': 'date'}),
#             'time': forms.TimeInput(attrs={'type': 'time'})}

#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         for field in self.Meta.fields:
#             self.fields[field].widget.attrs.update({
#                 'class': 'form-control'
#             })
class inputjadwal(forms.Form):
    nama_kegiatan = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Kegiatan',
        'type' : 'text',
        'required' : True
    }))
    date_kegiatan = forms.DateField(
        label = 'Date',
        widget = forms.SelectDateWidget(
            years = range(1945,2019,1)
        )
    )
    time_kegiatan = forms.TimeField(
        label = 'Time',
        widget=forms.TimeInput(format='%H:%M')
    )
    tempat_kegiatan = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Tempat Kegiatan',
        'type' : 'text',
        'required' : True
    }))
    kategori_kegiatan = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Kategori Kegiatan',
        'type' : 'text',
        'required' : True
    }))

class deleteJadwal(forms.Form):
    namadelete = forms.CharField(label='Nama yang dihapus:')
    